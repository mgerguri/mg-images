import Vue from 'vue'
import { ThumbsUpIcon, DownloadIcon, StarIcon, XIcon } from 'vue-feather-icons'

Vue.component('LikeIcon', ThumbsUpIcon)
Vue.component('DownloadIcon', DownloadIcon)
Vue.component('StarIcon', StarIcon)
Vue.component('XIcon', XIcon)
