export const sortFunction = (arrayToSort, sortWay, sortField) => {
    if (arrayToSort) {
        return arrayToSort.sort(function (a, b) {
            if (a[sortField] < b[sortField]) {
                return sortWay === 'asc' ? -1 : 1;
            }
            if (a[sortField] > b[sortField]) {
                return sortWay === 'asc' ? 1 : -1;
            }
            return 0;

        })
    }
}
