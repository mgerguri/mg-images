import axios from 'axios'

let base_url = 'https://pixabay.com/api/'

export default {
    searchImages(search_query) {
        let params = {
            key: process.env.VUE_APP_API_KEY,
            per_page: 52,
            q: encodeURIComponent(search_query)
        }

        return axios.get(base_url, { params })
    }
}
