import Axios from 'axios'
import { Notification } from "element-ui";

Axios.defaults.headers.common.Accept = 'application/json'

Axios.interceptors.response.use((response) => {

    return response;
}, function (error) {
    // Do something with response error

    console.log('complete error', error)

    Notification.error({
        title: 'Error',
        message: error
    })

    return Promise.reject(error);
});

Axios.interceptors.request.use((config) => {

    return config;
}, function (error) {
    // Do something with request error

    return Promise.reject(error);
});
