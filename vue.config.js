module.exports = {
    css: {
        loaderOptions: {
            sass: {
                prependData: `@import 'src/assets/style/element-variables.scss';`
            }
        }
    },
    devServer: {
        watchOptions:{
            poll:true
        }
    }
}
